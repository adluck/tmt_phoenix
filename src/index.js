import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch, NavLink, browserHistory } from "react-router-dom";
import tmtLogo from "./img/logo.svg";
import Home from "./js/components/Home";
import DigitalHealth from "./js/components/Digitalhealth";
import css from './css/style.css';  
import icons from "./css/fontawesome/css/all.min.css";
import Ecosystem from "./js/components/Ecosystem";
import TagManager from "./js/components/Tagmanager";
import Error from "./js/components/Error"
 
const Main = document.getElementById("mdh");
Main ? ReactDOM.render(
    <BrowserRouter>

      <Switch>
        <Route path="/" component={DigitalHealth} exact />
        <Route path="/Ecosystem" component={Ecosystem} />
        <Route exact path="/DigitalHealth" component={DigitalHealth}  />
        <Route exact path="/TagManager" component={TagManager}  />
        <Route component={Error} />
      </Switch>
    
      </BrowserRouter>
    , Main) : false;