import React, { Component } from "react";
import { Link } from "react-router-dom";
import { BrowserRouter, Route, Switch, NavLink } from "react-router-dom";

class Digitalhealth extends Component {
  constructor() {
    super();
    this.state = {
      title: "My Digital Health"
    };
  }
  componentDidMount() {
    document.title = "My Digital Health";
  }
  render() {
    return (
      <div>
        <div className="mdh_toph">
          <div className="mdh_lhead">
            <div className="mdh_logo_cont">
              <img src="./src/img/logo.svg" alt="logoval" />
            </div>
            <div className="mdh_border_sep" />
            <div className="mdh_titl">
              <h2>{this.state.title}</h2>
            </div>
          </div>
          <div className="mdh_mhead">
            <div className="mdh_mid_btn_nav">
              <NavLink className="state" activeClassName="on" to="" exact>
                Welcome
              </NavLink>
              <NavLink className="state" activeClassName="on" to="/Ecosystem">
                My Ecosystem
              </NavLink>
              <NavLink
                className="state"
                activeClassName="on"
                to="/Digitalhealth"
              >
                My Digital Health
              </NavLink>
            </div>
          </div>
          <div className="mdh_rhead">
            <div className="mdh_right_icons">
              <i className="fas fa-envelope-open" />
              <i className="fas fa-users" />
              <i className="fas fa-file-upload" />
              <i className="fas fa-user" />
            </div>
          </div>
        </div>

        <div className="mdh_main">
          <div className="mdh_left_nav">
            <div className="mdh_left_nav_icons">
              <div className="set on">
                <img src="./src/img/overview.svg" className="img-icons-list" />
                <div className="selected">
                  <div className="shadow" />
                </div>
                <h4 className="ove">Overview</h4>
              </div>
              <div className="set">
                <img
                  src="./src/img/policymanager.svg"
                  className="img-icons-list"
                />
                <div className="selected">
                  <div className="shadow" />
                </div>
                <h4 className="pol">
                  Policy <br />
                  Manager
                </h4>
              </div>
              <div className="set">
                <img
                  src="./src/img/reportmanager.svg"
                  className="img-icons-list reports"
                />
                <div className="selected">
                  <div className="shadow" />
                </div>
                <h4 className="rep">
                  Report <br />
                  Manager
                </h4>
              </div>
              <div className="set">
                <NavLink to="/TagManager">
                  <img
                    src="./src/img/tagmanager.svg"
                    className="img-icons-list tag"
                  />
                </NavLink>

                <div className="selected">
                  <div className="shadow" />
                </div>
                <h4 className="tag">Tag Manager</h4>
              </div>
              <div className="setHelp">
                <i className="fas fa-question-circle" />
              </div>
            </div>
          </div>
          <div className="mdh_viewp" id="mdh_viewp">
            <div className="mdh_viewp_top_nav">
              <a href="javascript:void(0);">Ad Operations Report</a>
              <a href="javascript:void(0);">Programmatic Operation Report</a>
              <a href="javascript:void(0);">Advertiser Report 2</a>
            </div>
            <div className="mdh_box_views">
              <div className="mdh_box">
                <div className="mdh_dummy" />
                <div className="mdh_info">
                  <h3>Total Tags Scanned</h3>
                  <h1>86</h1>
                  <div className="mdh_bottom_row">
                    <div className="mdh_info_bottom green">
                      <i className="fas fa-long-arrow-alt-up" />
                      <span>38%</span>
                    </div>
                    <div className="mdh_info_graph">
                      <i className="fas fa-chart-bar" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mdh_box">
                <div className="mdh_dummy" />
                <div className="mdh_info">
                  <h3>Total Tags In Violation</h3>
                  <h1>700</h1>
                  <div className="mdh_bottom_row">
                    <div className="mdh_info_bottom red">
                      <i className="fas fa-long-arrow-alt-down" />
                      <span>18%</span>
                    </div>
                    <div className="mdh_info_graph">
                      <i className="fas fa-chart-bar" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mdh_box">
                <div className="mdh_dummy" />
                <div className="mdh_info">
                  <h3>Total Campaigns Fulfilled</h3>
                  <h1>534</h1>
                  <div className="mdh_bottom_row">
                    <div className="mdh_info_bottom green">
                      <i className="fas fa-long-arrow-alt-up" />
                      <span>10%</span>
                    </div>
                    <div className="mdh_info_graph">
                      <i className="fas fa-chart-bar" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mdh_box">
                <div className="mdh_dummy" />
                <div className="mdh_info">
                  <h3>Total Tags Packages QA'd</h3>
                  <h1>100</h1>
                  <div className="mdh_bottom_row">
                    <div className="mdh_info_bottom red">
                      <i className="fas fa-long-arrow-alt-down" />
                      <span>8%</span>
                    </div>
                    <div className="mdh_info_graph">
                      <i className="fas fa-chart-bar" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Digitalhealth;
