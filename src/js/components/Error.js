import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, NavLink } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";

class Error extends Component {
  constructor() {
    super();
    this.state = {
      error: null,
      title: "Home Page"
    };
  }
  componentDidMount() {
    document.title = "Welcome";
  }
  render() {
    return (
      <div><h1>There was an error loading the page please go back</h1></div>
    );
  }
}
export default Error;
