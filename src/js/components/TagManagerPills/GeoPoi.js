import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter,
  Route,
  Switch,
  NavLink,
  withRouter
} from "react-router-dom";
import PropTypes from "prop-types";

class GeoPoi extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      state : this.props.responseData,
      poi: this.props.countryData,
      poiNode: null
    };
  }

  render() {
    
    if (this.state.poi) { 

      this.state.ctyNode = this.state.poi.map(data => (
      <div
            className={
              this.props.selectedIDs.indexOf(data.id) > -1
                ? "pick_pill sel"
                : "pick_pill"
            }
            id={data.id}
            key={data.id}
          >
            <label className="click_cont">
              <input
                type="checkbox"
                name={"name_"+data.id}
                onClick={(e) => {this.props.AddPill(data.id, data.name, event)}}
                id="44433"
              />
              <span className="checkmark" />
            </label>
            <span className="pill_title"> {data.name} </span>
            {data.hasChildren ? <span
              className={
                this.props.countryActive === data.id
                  ? "pill_drill_down active"
                  : "pill_drill_down"
              }
              onClick={(e) => {this.props.ViewDrillDown(data.id, event)}}
              name="poi"
            >
              <i className="fas fa-expand" />
            </span> : null }
          </div>
      ));
    }
    return (
           <div className="geo_generate">

        <div className="geo_header">
          <h3 className="geo_label">Add Poi:</h3>
          <i className="fas fa-globe" />
          <input type="text" name="" id="pill_it_search" />
        </div>
        <div className="geo_view_pills">
   
        {this.state.staNode}
        </div>

    </div>
    );
  }
}

GeoPoi.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};
export default GeoPoi;
