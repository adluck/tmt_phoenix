import React from 'react';
import PropTypes from 'prop-types';

class Modal extends React.Component {
  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
      return null;
    }

    // The gray background
    const backdropStyle = {
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.3)',
      padding: 120,
      zIndex: 3
    };

    // The modal "window"
    const modalStyle = {
      position: "relative",
      backgroundColor: '#fff',
      borderRadius: 5,
      width: '85vw',
      height: '70vh',
      margin: '0 auto',
      padding: 30,
      zIndex: 5
    };

    return (
      <div className="backdrop" style={backdropStyle}  >
        <div className="modal tag_new_modal" style={modalStyle}>
          <div className="header">
 					
         		<i className="fas fa-times" onClick={this.props.onClose}></i>
          </div>
          {this.props.children}

        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default Modal;