import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter,
  Route,
  Switch,
  NavLink,
  withRouter
} from "react-router-dom";

import { createStore } from "redux";
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";
import TagDialog from "./TagModal.js";
import EditTagDialog from "./EditTagModal.js";
import BasicEditInfoTag from "./utility/BasicEditInfoTag.js";
import GeoEditSnip from "./utility/GeoEditSnip.js";
import CookieEditSnip from "./utility/CookieEditSnip.js";
import DeviceEditSnip from "./utility/DeviceEditSnip.js";
import OsEditSnip from "./utility/OsEditSnip.js";
import BrowserEditSnip from "./utility/BrowserEditSnip.js";
import GeoSnip from "./utility/GeoSnip.js";
import DatePicker from "./utility/DatePicker.js";
import BasicInfoTag from "./utility/BasicInfoTag.js";
import CookieSnip from "./utility/CookieSnip.js";
import OsSnip from "./utility/OsSnip.js";
import BrowserSnip from "./utility/BrowserSnip.js";
import DeviceSnip from "./utility/DeviceSnip.js";
import ajaxCall from "./utility/ajaxCall.js";

const styles = theme => ({
  form: {
    display: "flex",
    flexDirection: "column",
    margin: "auto",
    width: "fit-content"
  },
  formControl: {
    marginTop: theme.spacing.unit * 2,
    minWidth: 120
  },
  formControlLabel: {
    marginTop: theme.spacing.unit
  }
});

class Tagmanager extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      path: "tagmanager",
      ajaxtest: null,
      ajaxURL: "/",
      error: null,
      isLoaded: false,
      datas: [],
      pagination: [],
      title: "Tag Manager",
      allMyData: null,
      maxWidth: "xl",
      isOpen: false,
      isOpenEdit: false,
      loadEdit: false,
      scroll: "paper",
      name_s: "start_date",
      name_e: "end_date",
      showMyGeo: false,
      showMyCookie: false,
      showMyBasic: true,
      showMyOS: false,
      showMyBrowser: false,
      showMyDevice: false,
      editShowMyBasic: true,
      editShowMyGeo: false,
      editShowMyCookie: false,
      editShowMyOS:false,
      editShowMyBrowser:false,
      editShowMyDevice:false,
      editIsLoaded: false,
      editData: []
    };
    this.ToggleSetState = this.ToggleSetState.bind(this);
  }

  handleClick = (event, param) => {
    fetch("../sample_data/tag-edit.json", {
      method: "GET",
      dataType: "JSON",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            editIsLoaded: true,
            editData: result
          });
        },
        error => {
          this.setState({
            editIsLoaded: false,
            error
          });
        }
      );
    this.toggleModalEdit();
  };
  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };
  toggleModalEdit = () => {
    this.setState({
      isOpenEdit: !this.state.isOpenEdit
    });
  };
  ToggleSetState = (array) =>{
    console.log(this.state);
     array.forEach((key, index, value) => {
      for(var a in key){
        var temp = a;
        this.setState({
          [temp] : key[a]
        });
      }
    });
  }
  ToggleBasicModalView(e){
    var statusArray = [
      {"showMyBasic":true},
      {"showMyCookie":false},
      {"showMyOS":false},
      {"showMyGeo":false},
      {"showMyDevice":false},
      {"showMyBrowser":false}
    ];
    e.preventDefault();

    var status = e.target.name;
    statusArray.forEach(function(key, index, value){
      for(var a in key){
       if(status == a){
        key[a] = true;
       } else {
         key[a] = false;
       }
      }

    });
    this.ToggleSetState(statusArray);
  }
    ToggleEditModalView(e){
    var statusArray = [
      {"editShowMyBasic":true},
      {"editShowMyCookie":false},
      {"editShowMyOS":false},
      {"editShowMyGeo":false},
      {"editShowMyDevice":false},
      {"editShowMyBrowser":false}
    ];
    e.preventDefault();

    var status = e.target.name;
    statusArray.forEach(function(key, index, value){
      for(var a in key){
       if(status == a){
        key[a] = true;
       } else {
         key[a] = false;
       }
      }

    });
    this.ToggleSetState(statusArray);
  }
 
  toggle() {
    e.nativeEvent.stopImmediatePropagation();
  }
  componentDidMount() {

    var myResolve = responseData => {
      this.setState({
        datas: responseData.data
      });
    };


    var myReject = function(reject) {
      console.log(reject);
    };
    //Parameters
    let dataFile = "tags-data.json";
    let path = "../sample_data/" + dataFile;
    let queryParams = {
      pageSize: 25,
      offset: 250
    };
    let options = {};
    new Promise((resolve, reject) => {
      ajaxCall
        .get(path, queryParams, options)
        .then(responseData => myResolve(responseData));
    });
  }
  render() {
        console.log(this.state.datas);
    const { error, datas, pagination, title, editData } = this.state;
    if (error) {
      return <div>Error: {error.message} </div>;
    } else {
      this.state.allMyData = datas.map(data => (
        <div className="tag_tags_data" key={data.md5sum}>
          <label className="click_cont">
            <input type="checkbox" />
            <span className="checkmark" />
          </label>
          <div className="tag_tag">
            <div className="tag_holder">
              <div className="tag_list">
                <div className="tag_name">{data.name}</div>
                <div className="tag_activeDate">
                  Active Date: {data.start_date} to {data.end_date}
                </div>
                <div className="tag_lastScan">Last Scan: {data.last_scan}</div>
                <div className="tag_status">
                  <i className="fas fa-check" />
                  {data.status == 1 ? "Active" : "oh no"}
                </div>
              </div>
              <div className="tag_actions">
                <button className="tag_all">All Results</button>
                <button className="tag_summ">Summary</button>
                <button
                  className="tag_edit"
                  onClick={e => {
                    this.handleClick(e, data.md5sum);
                  }}
                >
                  Edit
                </button>
              </div>
            </div>
          </div>
        </div>
      ));
    }
    return (
      <div>
        <div>
          <div className="mdh_toph">
            <div className="mdh_lhead">
              <div className="mdh_logo_cont">
                <img src="./src/img/logo.svg" alt="logoval" />
              </div>
              <div className="mdh_border_sep" />
              <div className="mdh_titl">
                <h2>{this.state.title}</h2>
              </div>
            </div>
            <div className="mdh_mhead">
              <div className="mdh_mid_btn_nav">
                <NavLink className="state" activeClassName="on" to="" exact>
                  Welcome
                </NavLink>
                <NavLink className="state" activeClassName="on" to="/Ecosystem">
                  My Ecosystem
                </NavLink>
                <NavLink
                  className="state"
                  activeClassName="on"
                  to="/DigitalHealth"
                >
                  My Digital Health
                </NavLink>
              </div>
            </div>
            <div className="mdh_rhead">
              <div className="mdh_right_icons">
                <i className="fas fa-envelope-open" />
                <i className="fas fa-users" />
                <i className="fas fa-file-upload" />
                <i className="fas fa-user" />
              </div>
            </div>
          </div>
          <div className="mdh_main">
            <div className="mdh_left_nav">
              <div className="mdh_left_nav_icons">
                <div className="set">
                  <img
                    src="./src/img/overview.svg"
                    className="img-icons-list"
                  />
                  <div className="selected">
                    <div className="shadow" />
                  </div>
                  <h4 className="ove">Overview</h4>
                </div>
                <div className="set">
                  <img
                    src="./src/img/policymanager.svg"
                    className="img-icons-list"
                  />
                  <div className="selected">
                    <div className="shadow" />
                  </div>
                  <h4 className="pol">
                    Policy <br />
                    Manager
                  </h4>
                </div>
                <div className="set">
                  <img
                    src="./src/img/reportmanager.svg"
                    className="img-icons-list reports"
                  />
                  <div className="selected">
                    <div className="shadow" />
                  </div>
                  <h4 className="rep">
                    Report <br />
                    Manager
                  </h4>
                </div>
                <div className="set on">
                  <img
                    src="./src/img/tagmanager.svg"
                    className="img-icons-list tag"
                  />
                  <div className="selected">
                    <div className="shadow" />
                  </div>
                  <h4 className="tag">Tag Manager</h4>
                </div>
                <div className="setHelp">
                  <i className="fas fa-question-circle" />
                </div>
              </div>
            </div>
            <div className="tag_viewp" id="tag_viewp">
              <div className="tag_toph_nav">
                <div className="tag_filter">
                  <button className="active">Active</button>
                  <button>Inactive</button>
                  <button>All</button>
                </div>
                <div className="tag_toph">
                  <div className="tag_toph_first">
                    <div className="tag_toph_first_hold">
                      <h2>Current Number of active tags</h2>
                    </div>
                    <div className="tag_toph_first_hold">
                      <h1>256</h1>
                    </div>
                  </div>
                  <div className="tag_seperator" />
                  <div className="tag_toph_second">
                    <div className="tag_toph_second_hold">
                      <h2>Number of Tags Scanned Over Past 7 Days</h2>
                    </div>
                    <div className="tag_toph_second_hold">
                      <h1>56</h1>
                    </div>
                  </div>
                </div>
                <div className="tag_options_row">
                  <div className="tag_left_tag_options">
                    <div className="tag_sel_all">
                      <label className="click_container">
                        Select All
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    </div>
                    <div className="tag_mass_edit">
                      <button>Mass Edit</button>
                    </div>
                    <div className="add_new_tag">
                      <button
                        className="tag_add_new"
                        onClick={this.toggleModal}
                      >
                        Add New Tag
                        <i className="fas fa-tags" />
                      </button>
                    </div>
                  </div>
                  <div className="tag_center_tag_options">
                    <div className="tag_pagination">
                      <div className="tag_list">1-10 of 56</div>
                      <div className="tag_lr">
                        <span className="left-dir">&lt;</span>
                        <span className="right-dir click">&gt;</span>
                      </div>
                      <div className="tag_sel_listings">
                        <select>
                          <option>10</option>
                        </select>
                        <span>Per page</span>
                      </div>
                    </div>
                  </div>
                  <div className="tag_right_tag_options">
                    <span>
                      <select name="sortby" className="tag_options_sortby">
                        <option value="">Sort By</option>
                        <option value="">Sort By</option>
                        <option value="fstart">First Start</option>
                        <option value="lstart">Last Start</option>
                        <option value="fexp">First Expiration</option>
                        <option value="lexp">Last Expiration</option>
                        <option value="az">A to Z</option>
                        <option value="za">Z to A</option>
                      </select>
                    </span>
                    <span className="tag_search_filter">
                      <input
                        name="search_tag_name"
                        className="tag_search"
                        type="text"
                        placeholder="Search Tag Name"
                      />
                      <i className="fas fa-search" />
                    </span>
                  </div>
                </div>
                <div className="tag_all_tags">
                  <div className="tag_row_tags">{this.state.allMyData}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <TagDialog show={this.state.isOpen} onClose={this.toggleModal}>
          <div className="tag_develop" width={this.state.maxWidth}>
            <form id="tag_dev_create">
              {this.state.showMyBasic ? <BasicInfoTag /> : null}
              {this.state.showMyGeo ? <GeoSnip /> : null}
              {this.state.showMyCookie ? <CookieSnip /> : null}
              {this.state.showMyOS ? <OsSnip /> : null }
              {this.state.showMyBrowser ? <BrowserSnip /> : null }
              {this.state.showMyDevice ? <DeviceSnip /> : null }
              <div className="tag_nav_buttons">
                {this.state.showMyBasic !== true ? (
                  <button
                    className="tag_nav_btn goback" name="showMyBasic"
                    onClick={this.ToggleBasicModalView.bind(this)}
                  >
                    <i className="fas fa-chevron-left" />
                    Go Back
                  </button>
                ) : null}
                {this.state.showMyCookie !== true ? (
                  <button
                    className="tag_nav_btn coo" name="showMyCookie"
                    onClick={this.ToggleBasicModalView.bind(this)}
                  >
                    Add Cookies
                    <i className="fas fa-cookie-bite" />
                  </button>
                ) : null}
                {this.state.showMyGeo !== true ? (
                  <button
                    className="tag_nav_btn geo" name="showMyGeo"
                    onClick={this.ToggleBasicModalView.bind(this)}
                  >
                    Add Geo
                    <i className="fas fa-globe-americas" />
                  </button>
                ) : null}
                {this.state.showMyOS !== true ? (
                  <button
                    className="tag_nav_btn geo" name="showMyOS"
                    onClick={this.ToggleBasicModalView.bind(this)}
                  >
                    Add OS
                    <i className="fas fa-window-restore"></i>
                  </button>
                ) : null}
                {this.state.showMyBrowser !== true ? (
                  <button
                    className="tag_nav_btn geo" name="showMyBrowser"
                    onClick={this.ToggleBasicModalView.bind(this)}
                  >
                    Add Browser
                    <i className="fas fa-desktop" />
                  </button>
                ) : null}
                {this.state.showMyDevice !== true ? (
                  <button
                    className="tag_nav_btn geo" name="showMyDevice"
                    onClick={this.ToggleBasicModalView.bind(this)}
                  >
                    Add Device
                    <i className="fas fa-mobile-alt"></i>
                  </button>
                ) : null}
              </div>
              <div className="footer">
                <button>
                  Apply Changes
                  <i className="fas fa-save" />
                </button>
                <button>
                  Check All URLs
                  <i className="fas fa-unlink" />
                </button>
              </div>
            </form>
          </div>
        </TagDialog>
        <EditTagDialog
          show={this.state.isOpenEdit}
          onClose={this.toggleModalEdit}
        >
          <div className="tag_develop" width={this.state.maxWidth}>
            <form id="tag_dev_create">
             {this.state.editShowMyBasic ? <BasicEditInfoTag tagData={editData} key={editData.tag_name} /> : null}
              {this.state.editShowMyGeo ?  <GeoEditSnip tagData={editData} key={editData.tag_name} />: null}
              {this.state.editShowMyCookie ? <CookieEditSnip tagData={editData} key={editData.tag_name} /> : null}
              {this.state.editShowMyOS ? <OsEditSnip tagData={editData} key={editData.tag_name}/> : null }
              {this.state.editShowMyBrowser ? <BrowserEditSnip tagData={editData} key={editData.tag_name} /> : null }
              {this.state.editShowMyDevice ? <DeviceEditSnip tagData={editData} key={editData.tag_name} /> : null }
             
              <div className="tag_nav_buttons">
               {this.state.showMyBasic !== true ? (
                  <button
                    className="tag_nav_btn goback" name="editShowMyBasic"
                    onClick={this.ToggleEditModalView.bind(this)}
                  >
                    <i className="fas fa-chevron-left" />
                    Go Back
                  </button>
                ) : null}
                {this.state.showMyCookie !== true ? (
                  <button
                    className="tag_nav_btn coo" name="editShowMyCookie"
                    onClick={this.ToggleEditModalView.bind(this)}
                  >
                    Add Cookies
                    <i className="fas fa-cookie-bite" />
                  </button>
                ) : null}
                {this.state.showMyGeo !== true ? (
                  <button
                    className="tag_nav_btn geo" name="editShowMyGeo"
                    onClick={this.ToggleEditModalView.bind(this)}
                  >
                    Add Geo
                    <i className="fas fa-globe-americas" />
                  </button>
                ) : null}
                {this.state.showMyOS !== true ? (
                  <button
                    className="tag_nav_btn geo" name="editShowMyOS"
                    onClick={this.ToggleEditModalView.bind(this)}
                  >
                    Add OS
                    <i className="fas fa-window-restore"></i>
                  </button>
                ) : null}
                {this.state.showMyBrowser !== true ? (
                  <button
                    className="tag_nav_btn geo" name="editShowMyBrowser"
                    onClick={this.ToggleEditModalView.bind(this)}
                  >
                    Add Browser
                    <i className="fas fa-desktop" />
                  </button>
                ) : null}
                {this.state.showMyDevice !== true ? (
                  <button
                    className="tag_nav_btn geo" name="editShowMyDevice"
                    onClick={this.ToggleEditModalView.bind(this)}
                  >
                    Add Device
                    <i className="fas fa-mobile-alt"></i>
                  </button>
                ) : null}
              </div>
              <div className="footer">
                <button>
                  Apply Changes
                  <i className="fas fa-save" />
                </button>
                <button>
                  Check All URLs
                  <i className="fas fa-unlink" />
                </button>
              </div>
            </form>
          </div>
        </EditTagDialog>
      </div>
    );
  }
}
export default Tagmanager;
