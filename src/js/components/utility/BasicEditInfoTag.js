import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter, 
  Route,
  Switch,
  NavLink,
  withRouter
} from "react-router-dom";
import PropTypes from 'prop-types';
import DatePicker from "./DatePicker.js";


class BasicEditInfoTag extends React.Component { 
  constructor(props) {
      super(props);
      this.state = {
        name_tag:this.props.tagData.tag_name,
        date_start: this.props.tagData.date_start,
        date_end: this.props.tagData.date_end,
        tag: this.props.tagData.tag,
        name_s: "start_date",
        name_e: "end_date",
        url_ref: this.props.tagData.referrer_url,
        scans_per_day: this.props.tagData.scans_per_day,
        partner_name: this.props.tagData.partner_name,
        urlNode: null
      },
      this.handleInputChange = this.handleInputChange.bind(this);
      this.handleTextAreaChange = this.handleTextAreaChange.bind(this);
      this.input = React.createRef();
  }

 handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    console.log(name);
    this.setState({ 
      [name]: value
    });
  }
  handleTextAreaChange(event){
      this.setState({ tag_code: event.target.value
    });
  }

  render() {
    const urlmap = this.state.url_ref;
    const backdropStyle = {
      position: 'fixed', 
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.3)',
      padding: 120,
      zIndex: 3
    };

      if(this.state.url_ref){
            this.state.urlNode = urlmap.map((url, i) => (
<div className='url_listing' key={i}><span>{url}</span><i className="fas fa-times-circle"></i></div>
            ));
         }
    return (
<div className='tag_fill_basic'>
   <div className="tag_n">
      <h1>Add New Tag (Required) </h1>
      <div className='tag_name_tag'>
         <label>Name of Tag</label>
         <i className="fas fa-tags"></i>
         <input type='text' name='name_tag' className='name_tag' placeholder="Enter tag name" value={this.state.name_tag} onChange={this.handleInputChange} />
      </div>
      <div className='tag_date_range'>
         <label>Date Range</label>
         <div className='tag_date_holders start'>
            <i className="fas fa-calendar-alt"></i>
            <DatePicker name="{this.state.name_s}" placeholder="Enter start date" dateDataStart={this.state.date_start} value={this.state.date_start}/>
         </div>
         <div className='tag_date_holders end'>
            <i className="fas fa-calendar-alt"></i>
            <DatePicker name="end_date" placeholder="Enter end date" dateDataEnd={this.state.date_end}/>
         </div>
      </div>
      <div className='tag_code_entry'>
         <label>Tag Code Entry</label>
         <i className="fas fa-code"></i>
         <textarea placeholder="Enter tag code" name='tag_code' onChange={this.handleTextAreaChange} value={this.state.tag} ></textarea>
      </div>
   </div>
   <div className="tag_advanced">
      <h1>Advanced Rules (optional) </h1>
      <div className='tag_advanced_encryp'>
         <span className='tag_scan_question'>Scan this tag for Encryption Violations?</span>
         <span className='tag_advanced_radio'>
            <input type='radio' name='encryption_violations' value="yes" id="encryp-option-yes"/><label htmlFor="encryp-option-yes">Yes</label>
            <div className='check'></div>
         </span>
         <span className='tag_advanced_radio'>
            <input type='radio' name='encryption_violations' value="no" id="encryp-option-no"/><label htmlFor='encryp-option-no'>No</label>
            <div className='check'></div>
         </span>
      </div>
      <div className='adv_scan_cap'>
         <label>Scans Per Day Cap</label> 
         <i className="fas fa-barcode"></i>
         <input type='text' name='scans_per_day' className='adv_scans_per_day' placeholder="Enter scans per day cap" value={this.state.scans_per_day} onChange={this.handleInputChange} />
      </div>
      <div className='adv_part_name'>
         <label>Partner</label>
         <div className='tag_date_holders start'> <i className="fas fa-handshake"></i><input type="text" name="partner_name" placeholder="Enter name of partner" value={this.state.partner_name} onChange={this.handleInputChange} /></div>
      </div>
      <div className='adv_url_entry'>
         <label>Referrer URLs</label>
         <i className="fas fa-link"></i>
         <input type="text" name="adv_refer_urls" placeholder="Enter referrer urls to the list"/>
         <label>Selected URLs</label>
         <div className="adv_build_urls">
          {this.state.urlNode}
         </div>
      </div>
   </div>
</div>
    );
  }
}

 BasicEditInfoTag.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default BasicEditInfoTag;

