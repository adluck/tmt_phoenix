import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from "./DatePicker.js";

class BasicInfoTag extends React.Component {
  render() {
    // Render nothing if the "show" prop is false

    // The gray background
    const backdropStyle = {
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.3)',
      padding: 120,
      zIndex: 3
    };



    return (
      <div className='tag_fill_basic'>
  <div className="tag_n">
   <h1>Add New Tag (Required) </h1>
   
   <div className='tag_name_tag'>
      <label>Name of Tag</label>
      <i className="fas fa-tags"></i>
      <input type='text' name='name_tag' className='name_tag' placeholder="Enter tag name"/>
   </div>
   <div className='tag_date_range'>
    <label>Date Range</label>
   <div className='tag_date_holders start'> <i className="fas fa-calendar-alt"></i><DatePicker name="{this.state.name_s}" placeholder="Enter start date" /></div><div className='tag_date_holders end'><i className="fas fa-calendar-alt"></i><DatePicker name="end_date" placeholder="Enter end date"/></div>
   </div>
   <div className='tag_code_entry'>
    <label>Tag Code Entry</label>
    <i className="fas fa-code"></i>
    <textarea placeholder="Enter tag code"></textarea>
   </div>
  </div>
  <div className="tag_advanced">
     <h1>Advanced Rules (optional) </h1> 
    <div className='tag_advanced_encryp'><span className='tag_scan_question'>Scan this tag for Encryption Violations?</span><span className='tag_advanced_radio'><input type='radio' name='encryption_violations' value="yes" id="encryp-option-yes"/><label htmlFor="encryp-option-yes">Yes</label><div className='check'></div></span><span className='tag_advanced_radio'><input type='radio' name='encryption_violations' value="no" id="encryp-option-no"/><label htmlFor='encryp-option-no'>No</label><div className='check'></div></span>
    </div>
     <div className='adv_scan_cap'>
      <label>Scans Per Day Cap</label>
      <i className="fas fa-barcode"></i>
      <input type='text' name='adv_scans_per_day' className='adv_scans_per_day' placeholder="Enter scans per day cap"/>
   </div>
   <div className='adv_part_name'>
    <label>Partner</label>
   <div className='tag_date_holders start'> <i className="fas fa-handshake"></i><input type="text" name="adv_partner" placeholder="Enter name of partner" /></div>
  </div>
   <div className='adv_url_entry'>
    <label>Referrer URLs</label>
    <i className="fas fa-link"></i>
    <input type="text" name="adv_refer_urls" placeholder="Enter referrer urls to the list"/>
    <label>Selected URLs</label>
    <div className="adv_build_urls">
    <div className='url_listing'><span>https://www.</span><span>hereistheurl</span><i className="fas fa-times-circle"></i></div>
    </div>
  </div>
  </div>
</div>
    );
  }
}

 BasicInfoTag.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default BasicInfoTag;

