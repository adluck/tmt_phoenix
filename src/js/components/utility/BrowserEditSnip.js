import React from "react";
import PropTypes from "prop-types";

class BrowserEditSnip extends React.Component {

  constructor(props) {
    super(props);
    (this.state = {
      name_tag: this.props.tagData.tag_name,
      browser: this.props.tagData.browser,
      browserNode: null
    }),
      (this.input = React.createRef());
  }

  render() {
    const browsermap = this.state.browser;

  if (this.state.browser) {
      this.state.browserNode = browsermap.map((browser, id, i) => (
        <div className="set_pill_chosen" id="" key={browser.id}>
          <span> {browser.name}</span>
          <span className="rem_pill">
            <input type="hidden" name="geo_id" value="{browser.id}" />
            <i className="fas fa-times-circle" />
          </span>
        </div>
      ));
    }

 
    return (
      <div className="tag_coo_listing">
        <div className="coo_select">
          <h1>Add Browsers</h1>
          <div className="coo_info">
            <i className="fas fa-info-circle" />
            <span>Click on the checkbox to add that browser</span>
          </div>
          <div className="coo_pill_parent">
            <div className="coo_generate">
              <div className="coo_header">
                <h3 className="coo_label">Add Browser:</h3>
                <i className="fas fa-globe" />
                <input type="text" name="" id="pill_it_search" />
              </div>
              <div className="coo_view_pills">
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
              </div>
            </div>
            <div className="coo_landed_pills">
              <div className="coo_header">
                <h3 className="coo_label">You Selected:</h3>
              </div>
              <div className="coo_view_pills">
                {this.state.browserNode}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BrowserEditSnip.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default BrowserEditSnip;
