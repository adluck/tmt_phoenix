import React from 'react';
import PropTypes from 'prop-types';

class BrowserSnip extends React.Component {
  render() {
    // Render nothing if the "show" prop is false

    // The gray background
    const backdropStyle = {
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.3)',
      padding: 120,
      zIndex: 3
    };



    return (
       <div className="tag_coo_listing" >
          <div className="coo_select"  >
          <h1>Add Browser</h1>
          <div className="coo_info"><i className="fas fa-info-circle"></i><span>Click on the checkbox to add that Browser</span></div>
          <div className='coo_pill_parent'>
            <div className='coo_generate'>
                <div className='coo_header'>
                  <h3 className='coo_label'>Add Browser:</h3>
                  <i className="fas fa-globe"></i>
                  <input type='text' name="" id='pill_it_search' />
                </div>
                <div className='coo_view_pills'>
                  <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>

                </div>
            </div>
            <div className='coo_landed_pills'>
              <div className='coo_header'>
                  <h3 className='coo_label'>You Selected:</h3>

              </div>
              <div className='coo_view_pills'>
                 <div className='set_pill_chosen' id="">
                    
                    <span> Chrome </span><span className="rem_pill"><i className="fas fa-times-circle"></i></span>
                  </div>
             
              </div>
            </div>
          </div>
        </div>
      </div>


    );
  }
}

BrowserSnip.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default BrowserSnip;