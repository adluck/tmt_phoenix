import React from "react";
import PropTypes from "prop-types";

class CookieEditSnip extends React.Component {

  constructor(props) {
    super(props);
    (this.state = {
      name_tag: this.props.tagData.tag_name,
      cookie: this.props.tagData.cookie,
      cookieNode: null
    }),
      (this.input = React.createRef());
  }

  render() {
    const cookiemap = this.state.cookie;

  if (this.state.cookie) {
      this.state.cookieNode = cookiemap.map((cookie, id, i) => (
        <div className="set_pill_chosen" id="" key={cookie.id}>
          <span> {cookie.name}</span>
          <span className="rem_pill">
            <input type="hidden" name="geo_id" value="{cookie.id}" />
            <i className="fas fa-times-circle" />
          </span>
        </div>
      ));
    }


    return (
      <div className="tag_coo_listing">
        <div className="coo_select">
          <h1>Add Cookies</h1>
          <div className="coo_info">
            <i className="fas fa-info-circle" />
            <span>Click on the checkbox to add that cookie</span>
          </div>
          <div className="coo_pill_parent">
            <div className="coo_generate">
              <div className="coo_header">
                <h3 className="coo_label">Add Cookie:</h3>
                <i className="fas fa-globe" />
                <input type="text" name="" id="pill_it_search" />
              </div>
              <div className="coo_view_pills">
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
              </div>
            </div>
            <div className="coo_landed_pills">
              <div className="coo_header">
                <h3 className="coo_label">You Selected:</h3>
              </div>
              <div className="coo_view_pills">
                {this.state.cookieNode}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CookieEditSnip.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default CookieEditSnip;
