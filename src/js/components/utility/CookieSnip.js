import React from 'react';
import PropTypes from 'prop-types';

class CookieSnip extends React.Component {

 componentDidMount() {

    var myResolve = responseData => {
      this.setState({
        datas: responseData.data
      });
    };

    var myReject = function(reject) {
      console.log(reject);
    };
    //Parameters
    let dataFile = "tags-data.json";
    let path = "../sample_data/" + dataFile;
    let queryParams = {
      pageSize: 25,
      offset: 250
    };
    let options = {};
    new Promise((resolve, reject) => {
      ajaxCall
        .get(path, queryParams, options)
        .then(responseData => myResolve(responseData));
    });
  }


  
  render() {
    // Render nothing if the "show" prop is false

    // The gray background




    return (
       <div className="tag_coo_listing" >
          <div className="coo_select"  >
          <h1>Add Cookies</h1>
          <div className="coo_info"><i className="fas fa-info-circle"></i><span>Click on the checkbox to add that cookie</span></div>
          <div className='coo_pill_parent'>
            <div class='coo_generate'>
                <div className='coo_header'>
                  <h3 className='coo_label'>Add Cookie:</h3>
                  <i class="fas fa-globe"></i>
                  <input type='text' name="" id='pill_it_search' />
                </div>
                <div className='coo_view_pills'>
                  <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>
                                    <div className='pick_pill' id="">
                     <label className="click_cont">
                     
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    <span className='pill_title'> United States</span>
                  </div>

                </div>
            </div>
            <div className='coo_landed_pills'>
              <div className='coo_header'>
                  <h3 className='coo_label'>You Selected:</h3>

              </div>
              <div className='coo_view_pills'>
                 <div className='set_pill_chosen' id="">
                    
                    <span> United States</span><span className="rem_pill"><i className="fas fa-times-circle"></i></span>
                  </div>
             
              </div>
            </div>
          </div>
        </div>
      </div>


    );
  }
}

CookieSnip.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default CookieSnip;