import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter, 
  Route,
  Switch,
  NavLink,
  withRouter
} from "react-router-dom";
import PropTypes from 'prop-types';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

class DatePicker extends React.Component { 
  constructor(props) {
super(props);
 this.state = {
        date_start: this.props.dateDataStart,
        date_end: this.props.dateDataEnd 
      }
}

 render() {
  return( <DayPickerInput onDayChange={day => console.log(day)} name=""  value={this.state.date_start ? this.state.date_start : this.state.date_end }/> );
	}
}

export default DatePicker;