import React from "react";
import PropTypes from "prop-types";

class DeviceEditSnip extends React.Component {

  constructor(props) {
    super(props);
    (this.state = {
      name_tag: this.props.tagData.tag_name,
      device: this.props.tagData.device,
      deviceNode: null
    }),
      (this.input = React.createRef());
  }

  render() {
    const devicemap = this.state.device;

  if (this.state.device) {
      this.state.deviceNode = devicemap.map((device, id, i) => (
        <div className="set_pill_chosen" id="" key={device.id}>
          <span> {device.name}</span>
          <span className="rem_pill">
            <input type="hidden" name="geo_id" value="{device.id}" />
            <i className="fas fa-times-circle" />
          </span>
        </div>
      ));
    }


    return (
      <div className="tag_coo_listing">
        <div className="coo_select">
          <h1>Add devices</h1>
          <div className="coo_info">
            <i className="fas fa-info-circle" />
            <span>Click on the checkbox to add that device</span>
          </div>
          <div className="coo_pill_parent">
            <div className="coo_generate">
              <div className="coo_header">
                <h3 className="coo_label">Add device:</h3>
                <i className="fas fa-globe" />
                <input type="text" name="" id="pill_it_search" />
              </div>
              <div className="coo_view_pills">
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
              </div>
            </div>
            <div className="coo_landed_pills">
              <div className="coo_header">
                <h3 className="coo_label">You Selected:</h3>
              </div>
              <div className="coo_view_pills">
                {this.state.deviceNode}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

DeviceEditSnip.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default DeviceEditSnip;
