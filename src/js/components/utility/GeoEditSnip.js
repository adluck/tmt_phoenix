import React from "react";
import PropTypes from "prop-types";

class GeoEditSnip extends React.Component {
  constructor(props) {
    super(props);
    (this.state = {
      name_tag: this.props.tagData.tag_name,
      geo: this.props.tagData.geo,
      geoNode: null
    }),
      (this.input = React.createRef());
  }

  render() {
    const geomap = this.state.geo;
    console.log(this.state.geo);
    if (this.state.geo) {
      this.state.geoNode = geomap.map((geo, id, i) => (
        <div className="set_pill_chosen" id="" key={geo.id}>
          <span> {geo.name}</span>
          <span className="rem_pill">
            <input type="hidden" name="geo_id" value="{geo.id}" />
            <i className="fas fa-times-circle" />
          </span>
        </div>
      ));
    }

    return (
      <div className="tag_geo_listing">
        <div className="geo_select">
          <h1>Add Geographical Location</h1>
          <div className="geo_info">
            <i className="fas fa-info-circle" />
            <span>Click on the checkbox to add the location</span>
          </div>
          <div className="geo_pill_parent">
            <div className="geo_generate">
              <div className="geo_header">
                <h3 className="geo_label">Add Region:</h3>
                <i className="fas fa-globe" />
                <input type="text" name="" id="pill_it_search" />
              </div>
              <div className="geo_view_pills">
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> Un2 States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
                <div className="pick_pill" id="">
                  <label className="click_cont">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <span className="pill_title"> United States</span>
                </div>
              </div>
            </div>
            <div className="geo_landed_pills">
              <div className="geo_header">
                <h3 className="geo_label">You Selected:</h3>
              </div>
              <div className="geo_view_pills">
                { this.state.geoNode }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

GeoEditSnip.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default GeoEditSnip;
