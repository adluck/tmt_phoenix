import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter,
  Route,
  Switch,
  NavLink,
  withRouter
} from "react-router-dom";
import PropTypes from "prop-types";
import ajaxCall from "./ajaxCall.js";
import GeoCountry from "../TagManagerPills/GeoCountry.js";
import GeoStates from "../TagManagerPills/GeoStates.js";
import GeoCities from "../TagManagerPills/GeoCities.js";
import GeoPoi from "../TagManagerPills/GeoPoi.js";

class Geo extends React.Component {
  constructor(props) {
    super(props);
    (this.state = {
      responseData: [],
      selectedIDs: [],
      selectedGEO: [],
      htmlGEO: null,
      country: null,
      countryShown: true,
      countryDrillSelect: false,
      countryActive: null,
      states: null,
      stateShown: false,
      stateDrillSelect: false,
      city: null,
      cityShown: false,
      cityDrillSelect: false,
      poi: null,
      poiShown: false,
      poiDrillSelect: false
    }),
      (this.input = React.createRef());
    this.ViewDrillDown = this.ViewDrillDown.bind(this);
    this.AddPill = this.AddPill.bind(this);
    this.RemovePill = this.RemovePill.bind(this);
    this.AjaxDrillDown = this.AjaxDrillDown.bind(this);
  }

  componentDidMount() {
    var myResolve = responseData => {
      this.setState({
        countryActive: true,
        country: responseData.geo
      });

    };

    var myReject = function(reject) {
      console.log(reject);
    };
    //Parameters
    let dataFile = "country-geo.json";
    let path = "../../sample_data/" + dataFile;
    let queryParams = {
      pageSize: 25,
      offset: 250
    };
    let options = {};
    new Promise((resolve, reject) => {
      ajaxCall
        .get(path, queryParams, options)
        .then(responseData => myResolve(responseData));
    });
  }

  AjaxDrillDown(id, trigger){
    var taskSwtich;
    if(trigger == "country"){
      console.log("country");
      this.state.stateShown=true;
      this.state.cityShown=false;
      this.state.poiShown=false;
      taskSwtich = "states";
      this.setState({
        stateShown: true
      });
    } else if(trigger == "states"){
      console.log("states");
      this.state.cityShown=true;
      this.state.poiShown=false;
       taskSwtich = "cities";
      this.setState({
        cityShown: true
      });
    } else if(trigger == "cities"){
      this.state.poiShown=false;
       taskSwtich = "poi";
        this.setState({
        poiShown: true
      });
    } else {
      console.log('made the else line 99');
      this.state.stateShown=false;
      this.state.cityShown=false;
      this.state.poiShown=false;
    }
    var myResolve = responseData => {
      console.log(responseData);
      this.setState({
        [taskSwtich]: responseData.data
      });
    };

    var myReject = function(reject) {
      console.log(reject);
    };
    //Parameters
    let dataFile = "jsonDataPuller.php";
    let path = "../../sample_data/" + dataFile;
    let queryParams = {
      pageSize: 25,
      offset: 250
    };
    let options = {};
    new Promise((resolve, reject) => {
      ajaxCall
        .get(path, queryParams, options)
        .then(responseData => myResolve(responseData));
    });
  }
  ViewDrillDown(id, e) {
    var showme = e.target.getAttribute('name');

    if (this.state.countryActive !== id) {
      this.setState({
        countryDrillSelect: true,
        countryActive: id
      });
      
    } 
      this.AjaxDrillDown(this.state.countryActive, showme)
    
  }
  AddPill(id, name, e) {

    var checked = e.target.checked;

    if (checked === true) {
      let selGEO = this.state.selectedGEO;
      selGEO.push({ id: id, name: name, target: e.target });
      this.setState(state => ({
        selectedIDs: [...this.state.selectedIDs, id],
        selectedGEO: selGEO
      }));
      console.log(this.state.selectedGEO);
    } else {
      var array = [...this.state.selectedIDs]; // make a separate copy of the array
      var index = array.indexOf(id);
      if (index !== -1) {
        array.splice(index, 1);
        this.setState({ selectedIDs: array });
      }
      this.RemovePill(id, name, event);
    }
    console.log(checked);
  }
  RemovePill(id, name, e) {
    console.log("we made remove");
    let selGEOr = this.state.selectedGEO;
    for (var i = selGEOr.length - 1; i >= 0; i--) {
      if (selGEOr[i].id == id) {
        console.log(selGEOr[i].hasOwnProperty("target"));
        if (
          selGEOr[i].hasOwnProperty("target") &&
          selGEOr[i].target.checked === true
        ) {
          console.log("inside of the checkfalse");
          selGEOr[i].target.checked = false;
        }
        selGEOr.splice(i, 1);
      }
    }

    var array = [...this.state.selectedIDs]; // make a separate copy of the array
    var index = array.indexOf(id);
    if (index !== -1) {
      array.splice(index, 1);
      this.setState({ selectedIDs: array });
    }
    this.setState(state => ({
      selectedGEO: selGEOr,
      htmlGEO: this.state.htmlGEO
    }));
  }
  render() {
console.log(this.state.cities);
   this.state.htmlGEO = this.state.selectedGEO.map(data => (
      <div className="set_pill_chosen" key={data.id}>
        <span> {data.name}</span>
        <span
          className="rem_pill"
          onClick={this.RemovePill.bind(this, data.id, data.name, event)}
        >
          <i className="fas fa-times-circle" />
        </span>
      </div>
    ));
    return (

      <div className="tag_geo_listing">
        <div className="geo_select">
          <h1>Add Geographical Location</h1>
          <div className="geo_info">
            <i className="fas fa-info-circle" />
            <span>Click on the checkbox to add the location</span>
          </div>
          <div className="geo_pill_parent">
            
              <GeoCountry
                key={this.state.country}
                selectedIDs={this.state.selectedIDs}
                countryData={this.state.country}
                AddPill={this.AddPill}
                ViewDrillDown={this.ViewDrillDown}
                countryActive={this.state.countryActive}
                RemovePill={this.RemovePill}
              />
         
            {this.state.stateShown !== false ? 
              <GeoStates 
                key={this.state.states}
                selectedIDs={this.state.selectedIDs}
                statesData={this.state.states}
                AddPill={this.AddPill}
                ViewDrillDown={this.ViewDrillDown}
                countryActive={this.state.countryActive}
                RemovePill={this.RemovePill}

              /> : null}
            {this.state.cityShown !== false ? <GeoCities 
                key={this.state.cities}
                selectedIDs={this.state.selectedIDs}
                cityData={this.state.cities}
                AddPill={this.AddPill}
                ViewDrillDown={this.ViewDrillDown}
                countryActive={this.state.countryActive}
                RemovePill={this.RemovePill}


              /> : null}
            {this.state.poiShown !== false ? <GeoPoi 
                key={this.state.poi}
                selectedIDs={this.state.selectedIDs}
                poiData={this.state.poi}
                AddPill={this.AddPill}
                ViewDrillDown={this.ViewDrillDown}
                countryActive={this.state.countryActive}
                RemovePill={this.RemovePill}

              /> : null}

          <div className="geo_landed_pills">
            <div className="geo_header">
              <h3 className="geo_label">You Selected:</h3>
            </div>
            <div className="geo_view_pills" id="">
              {this.state.htmlGEO}
            </div>
  
        </div>
      </div>
      </div>
      </div>
    );
  }
}

Geo.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default Geo;
