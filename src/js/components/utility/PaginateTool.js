             import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter, 
  Route,
  Switch,
  NavLink,
  withRouter
} from "react-router-dom";
import PropTypes from 'prop-types';
import Paginated from "react-js-pagination";

const propTypes = {
    items: PropTypes.PropTypes.array.isRequired,
    onChangePage: PropTypes.PropTypes.func.isRequired,
    initialPage: PropTypes.PropTypes.number    
}

const defaultProps = {
    initialPage: 1,
    pageSize: 10
}
class PaginateTool extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            pager: {}, 
            defaultPageSize: 10,
            paginatedPageViewHTML: null,
            setHtml: null
        };
        this.onChangeSelectTotalViewTags = this.onChangeSelectTotalViewTags.bind(this);
    }
    onChangeSelectTotalViewTags(event){
        var newSetView = event.target.value * 1;
        defaultProps.pageSize = newSetView;
        this.state.defaultPageSize = newSetView;
        this.setPage(this.props.initialPage);

        //this.getPager(this.state.pager.totalItems, this.state.pager.currentPage, newSetView);
    }
    componentWillMount() {
        if (this.props.items && this.props.items.length) {
            this.setPage(this.props.initialPage);
        }
    }
    componentDidUpdate(prevProps, prevState) {
        // reset page if items array has changed
        if (this.props.items !== prevProps.items) {
            this.setPage(this.props.initialPage);
        }
    }
    setPage(page) {       
        var items = this.props.items;
        var pager = this.state.pager;
        if (page < 1 || page > pager.totalPages) {
            return;
        }
        // get new pager object for specified page
        pager = this.getPager(items.length, page);

        // get new page of items from items array
        var pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);
        // update state
         this.setState({ pager: pager });
        // call change page function in parent component
        this.props.onChangePage(pageOfItems);
    }
    getViewTrack(totalItems, currentPage, pageSize){
        this.state.setHtml ="";
        var totalPages = Math.ceil(totalItems / pageSize);
        if(currentPage == 1){
            this.state.setHtml += currentPage+"-"+pageSize;        
        } else {
            if(currentPage === totalPages){
                this.state.setHtml += ((((currentPage - 1) * pageSize) + 1) +"-"+(totalItems));
            } else {
                this.state.setHtml += ((((currentPage - 1) * pageSize) + 1) +"-"+(currentPage * pageSize));                
            }
          
        }
        this.state.setHtml += " of "+totalItems;
        this.state.paginatedPageViewHTML = this.state.setHtml;
    }
    getPager(totalItems, currentPage, pageSize) {
        currentPage = currentPage || 1;
        pageSize = this.state.defaultPageSize;
        var totalPages = Math.ceil(totalItems / pageSize);
        var startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
        // create an array of pages to ng-repeat in the pager control
        var pages = [...Array((endPage + 1) - startPage).keys()].map(i => startPage + i);
        this.getViewTrack(totalItems, currentPage, pageSize);
        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }

    render() {
        var pager = this.state.pager;

        if (!pager.pages || pager.pages.length <= 1) {
            // don't display pager if there is only 1 page
           // return null;
           console.log('we did it');
        }

        return (

                <div className="tag_pagination">
                      <div className="tag_list">{this.state.paginatedPageViewHTML}</div>
                      <div className="tag_lr">
                        <a onClick={() => this.setPage(pager.currentPage - 1)}><span className={pager.currentPage === 1 ? 'left-dir' : 'left-dir click'}>&lt;</span></a>
                        <a onClick={() => this.setPage(pager.currentPage + 1)}><span className={pager.currentPage === pager.totalPages ? 'right-dir' : 'right-dir click'} >&gt;</span></a>
                      </div>
                      <div className="tag_sel_listings">
                        <select onChange={this.onChangeSelectTotalViewTags} value={this.state.defaultPageSize}>
                          <option value="10">10</option>
                          <option value="25">25</option>
                          <option value="40">40</option>
                          <option value="50">50</option>
                        </select>
                        <span>Per page</span>
                      </div>
                    </div>
          
        );
    }
}

PaginateTool.propTypes = propTypes;
PaginateTool.defaultProps = defaultProps;
export default PaginateTool;