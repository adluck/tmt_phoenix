 

import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter, 
  Route,
  Switch,
  NavLink,
  withRouter
} from "react-router-dom";
import PropTypes from 'prop-types';
import DatePicker from "./DatePicker.js";
import Pagination from "react-js-pagination";
import PaginateTool from "./PaginateTool.js";


class TagPaginate extends React.Component { 
  constructor(props) {
    super(props);
    (this.state = {
        paginateData: null,
        allTagsData: this.props.allTags,
        currentPage: 1,
        pageOfItems: [],
        exampleItems: null
    }),
      (this.input = React.createRef());
       this.onChangePage = this.onChangePage.bind(this);
  }


    onChangePage(pageOfItems) {
      this.setState({ pageOfItems: pageOfItems });
      this.state.paginateData = pageOfItems;
    }
   render() {
     const { pageOfItems } = this.state.pageOfItems;
      return (
              <div className="tag_add_indiv">
                <div className="tag_options_row">
                  <div className="tag_left_tag_options">
                    <div className="tag_sel_all">
                      <label className="click_container">
                        Select All
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    </div>
                    <div className="tag_mass_edit">
                      <button>Mass Edit</button>
                    </div>
                    <div className="add_new_tag">
                      <button 
                        className="tag_add_new"
                        onClick={this.props.toggleModal}
                      >
                        Add New Tag
                        <i className="fas fa-tags" />
                      </button>
                    </div>
                  </div>
                  <div className="tag_center_tag_options">
                    <PaginateTool items={this.state.allTagsData} onChangePage={this.onChangePage}/>
                  </div>
                  <div className="tag_right_tag_options">
                    <span>
                      <select name="sortby" className="tag_options_sortby">
                        <option value="">Sort By</option>
                        <option value="">Sort By</option>
                        <option value="fstart">First Start</option>
                        <option value="lstart">Last Start</option>
                        <option value="fexp">First Expiration</option>
                        <option value="lexp">Last Expiration</option>
                        <option value="az">A to Z</option>
                        <option value="za">Z to A</option>
                      </select>
                    </span>
                    <span className="tag_search_filter">
                      <input
                        name="search_tag_name"
                        className="tag_search"
                        type="text"
                        placeholder="Search Tag Name"
                      />
                      <i className="fas fa-search" />
                    </span>
                  </div>
                </div>
                <div className="tag_all_tags">
                  <div className="tag_row_tags">
                      {this.state.pageOfItems.map(data => (
        <div className="tag_tags_data" key={data.md5sum}>
          <label className="click_cont">
            <input type="checkbox" />
            <span className="checkmark" />
          </label>
          <div className="tag_tag">
            <div className="tag_holder">
              <div className="tag_list">
                <div className="tag_name">{data.name}</div>
                <div className="tag_activeDate">
                  Active Date: {data.start_date} to {data.end_date}
                </div>
                <div className="tag_lastScan">Last Scan: {data.last_scan}</div>
                <div className="tag_status">
                  <i className="fas fa-check" />
                  {data.status == 1 ? "Active" : "oh no"}
                </div>
              </div>
              <div className="tag_actions">
                <button className="tag_all">All Results</button>
                <button className="tag_summ">Summary</button>
                <button
                  className="tag_edit"
                  onClick={e => {
                    this.handleClick(e, data.md5sum);
                  }}
                >
                  Edit
                </button>
              </div>
            </div>
          </div>
        </div>
      ))}
                 </div>
                </div>
              </div>
              );
        }
      }
    TagPaginate.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.node
};

export default TagPaginate;