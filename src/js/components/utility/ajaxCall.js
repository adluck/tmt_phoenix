import React, { Component } from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";

var DataObject = null;

const ajaxCall = {
  getRequestKey: () => {
    var crypto = require("md5");
    let b = [];
    let webGLVendor;
    let webGLRenderer;
    let canvas = document.createElement("canvas");
    let gl =
      canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
    let ctx =
      canvas.getContext("webgl") || canvas.getContext("experimental-webgl");

    if (
      ctx.getSupportedExtensions().indexOf("WEBGL_debug_renderer_info") >= 0
    ) {
      webGLVendor = ctx.getParameter(
        ctx.getExtension("WEBGL_debug_renderer_info").UNMASKED_VENDOR_WEBGL
      );
      webGLRenderer = ctx.getParameter(
        ctx.getExtension("WEBGL_debug_renderer_info").UNMASKED_RENDERER_WEBGL
      );
    } else {
      webGLVendor = "n/a";
      webGLRenderer = "n/a";
    }

    b.push(window.navigator ? navigator.userAgent || "" : "");
    b.push(window.navigator ? navigator.platform || "" : "");
    b.push(window.navigator ? navigator.language || "" : "");
    b.push(new Date().getTimezoneOffset() || null);

    b.push(navigator.mimeTypes);

    b.push(gl.getParameter(gl.RED_BITS));
    b.push(+gl.getParameter(gl.RENDERER));
    b.push(gl.getParameter(gl.SHADING_LANGUAGE_VERSION));
    b.push(gl.getParameter(gl.STENCIL_BITS));
    b.push(gl.getParameter(gl.VENDOR));
    b.push(gl.getParameter(gl.VERSION));
    b.push(webGLVendor);
    b.push(webGLRenderer);

    let padlockStr = JSON.stringify(b);
    return crypto(padlockStr);
  },

  get: (url, queryParams, options) => {
    var hasKey = ajaxCall.getRequestKey();
    var trackData = null;
    return fetch(url, {
      method: "GET",
      dataType: "JSON",
      TMTBCompass: hasKey,
      queryParams: queryParams,
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        queryParams: queryParams,
        "TMT-BCompass": hasKey,
        Method: "GET",
        dataType: "JSON"
      }
    })
      .then(response => response.json())
      .then(responseData => {
        return responseData;
      })
      .catch(error => console.warn(error));
  }
};
export default ajaxCall;
